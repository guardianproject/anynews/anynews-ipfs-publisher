import { create, globSource } from "ipfs-http-client";
import AWS from "aws-sdk";
import { promises as fs } from "fs";
import path from "path";

const getIPNSKey = async (ipfs, keyRoot) => {
	const ipnsKeys = await ipfs.key.list();
	const listRes = ipnsKeys.find((key) => key.name === keyRoot);
	let ipnsKey = listRes?.id;

	if (!ipnsKey) {
		console.log("Generating new key");
		const genRes = await ipfs.key.gen(keyRoot, { type: "rsa", size: 2048 });
		ipnsKey = genRes.id;
	}

	console.log({ ipnsKey });
	return ipnsKey;
};

const rewriteConfig = async (bucket, keyRoot, ipnsKey) => {
	const file = path.join("/tmp", keyRoot, "config.json");
	const configText = await fs.readFile(file, "utf8");
	const s3BaseURL = `https://s3.amazonaws.com/${bucket}/${keyRoot}/`;
	const ipfsBaseURL = `${process.env.IPFS_GATEWAY_URL}/ipns/${ipnsKey}/`;
	const config = configText.replaceAll(s3BaseURL, ipfsBaseURL);
	await fs.writeFile(file, config);
};

const downloadFiles = async (s3, bucket, prefix, continuationToken) => {
	let data;
	try {
		data = await s3
			.listObjectsV2({
				Bucket: bucket,
				Prefix: prefix,
				ContinuationToken: continuationToken,
			})
			.promise();
	} catch (err) {
		console.error("S3 listObjects error:", err);
	}

	for (let i = 0; i < data.Contents.length; i++) {
		const item = data.Contents[i];
		const localPath = path.join("/tmp", item.Key);
		if (localPath.endsWith("/")) {
			continue;
		}

		try {
			const fileData = await s3
				.getObject({
					Bucket: bucket,
					Key: item.Key,
				})
				.promise();
			await fs.mkdir(path.dirname(localPath), { recursive: true });
			await fs.writeFile(localPath, fileData.Body);
			console.log(`File downloaded: ${localPath}`);
		} catch (err) {
			console.error("S3 getObject error:", item.Key);
		}
	}

	if (data.IsTruncated) {
		await downloadFiles(s3, bucket, prefix, data.NextContinuationToken);
	}
};

export const handler = async (event) => {
	const updatedIPNSKeys = {};
	const region = event.Records[0].awsRegion;
	const bucket = event.Records[0].s3.bucket.name;
	const publishedApps = process.env.PUBLISHED_APPS.split(",");
	const keyRoots = event.Records.reduce((acc, rec) => {
		const fileName = rec.s3.object.key.split("/").pop();
		const keyRoot = rec.s3.object.key.split("/")[0];
		if (fileName === "index.xml") {
			acc[keyRoot] = true;
		}
		return acc;
	}, {});
	const filteredKeyRoots = Object.keys(keyRoots).filter((keyRoot) =>
		publishedApps.includes(keyRoot)
	);

	const s3 = new AWS.S3({ region });
	const apiCreds = `${process.env.INFURA_API_KEY}:${process.env.INFURA_API_SECRET}`;
	const authorization = `Basic ${Buffer.from(apiCreds).toString("base64")}`;
	const infura = create({
		host: process.env.INFURA_API_URL,
		port: 5001,
		protocol: "https",
		headers: {
			authorization,
		},
	});
	const ipfs = create(new URL(process.env.IPFS_API_URL));

	for await (const keyRoot of filteredKeyRoots) {
		await downloadFiles(s3, bucket, keyRoot, null);
		const ipnsKey = await getIPNSKey(ipfs, keyRoot);
		await rewriteConfig(bucket, keyRoot, ipnsKey);

		const filePath = `/tmp/${keyRoot}`;
		let count = 0;

		for await (const file of infura.addAll(globSource(filePath, "**/*"), {
			cidVersion: 1,
			pin: true,
			wrapWithDirectory: true,
		})) {
			count++;
			console.log({ count, cid: file.cid, path: file.path });

			if (file.path === "") {
				const res = await ipfs.name.publish(file.cid, {
					key: keyRoot,
					ttl: "3m",
				});
				updatedIPNSKeys[keyRoot] = res.name;
			}
		}
	}

	return {
		status: 200,
		statusDescription: "OK",
		body: JSON.stringify(updatedIPNSKeys),
	};
};
