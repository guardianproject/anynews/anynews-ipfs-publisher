import { handler } from "./index.js";
import { promises as fs } from "fs";

const event = await fs.readFile("./event.json", "utf-8");
const parsedEvent = JSON.parse(event);
const result = await handler(parsedEvent);

console.log({ result });
